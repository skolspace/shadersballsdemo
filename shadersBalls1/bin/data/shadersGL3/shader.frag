#version 150

#define MAX_NUMBER_OF_CIRCLES 16

out vec4 outputColor;
uniform float time;
uniform vec2 points2d[MAX_NUMBER_OF_CIRCLES];
uniform int numberOfActiveCircles;

float distanceToLine(vec2 p1, vec2 p2, vec2 point) {
    float a = p1.y-p2.y;
    float b = p2.x-p1.x;
   
    return abs(a*point.x+b*point.y+p1.x*p2.y-p2.x*p1.y) / sqrt(a*a+b*b);
}

bool pointOnLineBetween(vec2 p1, vec2 p2, vec2 point){
    if (distanceToLine(p1, p2, point) < 2) {
        if (min(p1.x, p2.x) <=point.x && point.x <= max(p1.x, p2.x)
        && min(p1.y, p2.y) <=point.y && point.y <= max(p1.y, p2.y)) {
            return true;
        }
    }

    return false;
}

bool needToDraw(vec2 p, float radiusOfCicle){
    int numberOfActiveCircles = min(numberOfActiveCircles, MAX_NUMBER_OF_CIRCLES);
    
    for (int i=0; i < numberOfActiveCircles; i++) {
        if (distance(p, points2d[i].xy) < radiusOfCicle) {
            return true;
        }
    }

    for (int i=0; i < numberOfActiveCircles-1 ; i++) {
       if (pointOnLineBetween(points2d[i], points2d[i+1], p)) {
           return true;
       }
    }

    return false;
}

void main()
{
    float windowWidth = 1024.0;
    float windowHeight = 768.0;

    float r = gl_FragCoord.x / windowWidth;
    float g = gl_FragCoord.y / windowHeight;

    if (needToDraw(gl_FragCoord.xy, 20)){
        float b = (sin(time) + 1.0)/2.0; 
        float a = 1.0;
        outputColor = vec4(r, g, b, a);
    }
    else {
        outputColor = vec4(1.0, 0.0, 1.0, 0);
    }
}