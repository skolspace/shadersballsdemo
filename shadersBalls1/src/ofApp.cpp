#include "ofApp.h"
#include "ofGraphics.h"

#define SHADER_PATH "shadersGL3/shader"

#define MAX_NUMBER_OF_POINTS 16
//--------------------------------------------------------------
void ofApp::setup() {
	reloadShader();

	// create a bunch of random points
	float r = ofGetHeight() / 2;
	for (int i = 0; i < MAX_NUMBER_OF_POINTS; i++) {
		points2d.push_back(ofVec2f(ofRandomf() * r, ofRandomf() * r));
	}

	gui.setup();
	gui.add(numberOfActiveCircles.set("count", 12, 8, MAX_NUMBER_OF_POINTS));
}

void ofApp::reloadShader() {
	shader.unload();
	bool shadeLoaded = shader.load(SHADER_PATH);
}
//--------------------------------------------------------------
void ofApp::update() {
	float time = ofGetElapsedTimef();
	//float speed = 0.1;
	float width = ofGetWidth();
	float height = ofGetHeight();
	for (int i = 0; i < numberOfActiveCircles; i++) {
		float speed = 0.1;
		float posX = i * 16.1;
		float posY = i * 13.22;

		float x = width * ofNoise(time*speed + posX);
		float y = height * ofNoise(time*speed + posY);
		points2d[i] = ofVec2f(x, y);
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	gui.draw();

	shader.begin();
	shader.setUniform1f("time", ofGetElapsedTimef());
	shader.setUniform2fv("points2d", &points2d[0].x, MAX_NUMBER_OF_POINTS);
	shader.setUniform1i("numberOfActiveCircles", numberOfActiveCircles);

	ofSetColor(255);
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	shader.end();

	ofDrawBitmapString("fps: " + ofToString((int)ofGetFrameRate()), 10, 10);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	switch (key)
	{
	case 'f':
		ofToggleFullscreen();
		break;
	case 'r':
		reloadShader();
		break;
	default:
		break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
